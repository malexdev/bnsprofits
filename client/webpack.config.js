var path = require('path');
var webpack = require('webpack');

var prod = process.argv.some(function(arg) { return arg === '--prod' || arg === '--production'; });
var indexFile = './src/index';

var config = {
  devtool: 'eval',
  entry: [
    'webpack-dev-server/client?http://localhost:3001',
    'webpack/hot/only-dev-server',
    indexFile
  ],
  output: {
    path: path.join(__dirname, '..', 'server', 'public', 'js'),
    filename: 'bundle.js',
    publicPath: '/static/'
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin()
  ],
  resolve: {
    extensions: ['', '.js', '.jsx']
  },
  module: {
    loaders: [{
      test: /(\.js)|(\.jsx)$/,
      loaders: ['babel'],
      include: path.join(__dirname, 'src')
    }]
  }
};

var productionPlugin = new webpack.DefinePlugin({
  'process.env': {
    'NODE_ENV': JSON.stringify('production')
  }
});

if (prod) {
  console.log('Building in PROD mode');
  delete config.devtool;
  config.entry = [indexFile]
  config.plugins = [productionPlugin];
}

module.exports = config;
