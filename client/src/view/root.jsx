import React from 'react'
import Navigation from './navbar'

export default class Root extends React.Component {
  render() {
    return (
      <div>
        <Navigation/>
        <div className='container'>
          {this.props.children}
        </div>
      </div>
    )
  }
}
