import React from 'react'

export default class About extends React.Component {
  render() {
    return (
      <div>
        <h2>We're here to make you money in the excellent game of Blade and Soul.</h2>
        <p>Note: BNS profits is not affiliated with NCSoft or Blade and Soul in any way. NCSoft owns all copyrights to the game Blade and Soul.</p>
      </div>
    )
  }
}
