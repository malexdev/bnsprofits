import React from 'react'
import {Navbar, Nav, NavItem, NavDropdown} from 'react-bootstrap'
import {Link} from 'react-router'
import {LinkContainer} from 'react-router-bootstrap'

const navtree = {
  about: [
    'About Golden Soul',
    'Donate',
    'Contribute'
  ],
  guild: {
    gathering: [
      'Herbside Service',
      'Green Thumbs',
      'Tree Fellers',
      'Trapper\'s Alliance',
      'Fish Network',
      'Stonecutters',
      'Prospector\'s Union'
    ],
    crafting: [
      'Silver Cauldron',
      'Earthseers',
      'Merry Potters',
      'Acquired Taste',
      'Soul Wardens',
      'Forgekeepers',
      'Radiant Ring'
    ]
  },
  other: {
    custom: [
      'Create Custom Item',
      'Your Custom Items'
    ],
    daily: [
      'Profit Calculator'
    ]
  }
}

export default class Navigation extends React.Component {
  constructor(props) {
    super(props)
  }

  escapeSlug(name) {
    return name.replace(/\W/g, '').toLowerCase()
  }

  sourcedDropdown(type, subtype, title) {
    let source = navtree
    if (type) { source = source[type] }
    if (subtype) { source = source[subtype] }

    const items = source.sort().map(source => {
      const escapedName = this.escapeSlug(source)
      let path = []
      if (type) { path.push(type) }
      if (subtype) { path.push(subtype) }
      path.push(escapedName)
      return (
        <LinkContainer key={escapedName} to={{pathname: `/${path.join('/')}`}}>
          <NavItem>{source}</NavItem>
        </LinkContainer>
      )
    })
    return (
      <NavDropdown title={title} id={this.escapeSlug(title)}>
        {items}
      </NavDropdown>
    )
  }

  gatheringGuildDropdown() {
    return this.sourcedDropdown('guild', 'gathering', 'Gathering Guilds')
  }

  craftingGuildDropdown() {
    return this.sourcedDropdown('guild', 'crafting', 'Crafting Guilds')
  }

  customDropdown() {
    return this.sourcedDropdown('other', 'custom', 'Custom Items')
  }

  dailyDropdown() {
    return this.sourcedDropdown('other', 'daily', 'Daily Quests')
  }

  aboutDropdown() {
    return this.sourcedDropdown('about', null, 'About')
  }

  render() {
    return (
      <Navbar fixedTop>
        <Navbar.Header>
          <Navbar.Brand>
            <Link to='/'>Golden Soul</Link>
          </Navbar.Brand>
          <Navbar.Toggle />
        </Navbar.Header>
        <Navbar.Collapse>
          <Nav>
            {this.craftingGuildDropdown()}
            {this.gatheringGuildDropdown()}
            {this.customDropdown()}
            {this.dailyDropdown()}
          </Nav>
          <Nav pullRight>
            {this.aboutDropdown()}
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    )
  }
}
