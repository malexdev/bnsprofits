import React from 'react'
import { Button, Row, Col, Glyphicon } from 'react-bootstrap'

export default class Forgekeepers extends React.Component {
  render() {
    return (
      <div>
        <h2 className='center-text'>The Forgekeepers</h2>
        <Row>
          <Col sm={8} smOffset={2}>
            <Button block bsStyle='success'><Glyphicon glyph='plus'/> Add Item</Button>
          </Col>
        </Row>
      </div>
    )
  }
}
