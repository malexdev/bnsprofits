import React, { Component } from 'react';
import { Button, Jumbotron } from 'react-bootstrap';

export default class Home extends Component {
  render() {
    return (
      <div>
        <Jumbotron>
          <h1>Golden Soul</h1>
        </Jumbotron>
        <h4 className='center-text'>
          Welcome to Golden Soul. Hope you enjoy!
        </h4>
      </div>
    );
  }
}
