import React from 'react'
import { Panel, Col } from 'react-bootstrap'

export default class NotFound extends React.Component {
  render() {
    return (
      <Col xs={10} xsOffset={1}>
        <Panel bsStyle='danger'>
          Unfortunately, the resource requested does not exist.
        </Panel>
      </Col>
    )
  }
}
