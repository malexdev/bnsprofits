import { Router, Route, browserHistory, IndexRoute } from 'react-router'
import React from 'react'

import Root from './view/root.jsx'
import Home from './view/home.jsx'
import NotFound from './view/404.jsx'
import About from './view/about.jsx'

import Prospectors from './view/guild/gathering/prospectors'
import Stonecutters from './view/guild/gathering/stonecutters'
import Forgekeepers from './view/guild/crafting/forgekeepers'
import Radiant from './view/guild/crafting/radiant'

export default class App extends React.Component {
  render() {
    return (
      <Router history={browserHistory}>
        <Route path='/' component={Root}>
          <IndexRoute component={Home} />
          <Route path='about' component={About} />
          <Route path='guild'>
            <Route path='crafting'>
              <Route path='forgekeepers' component={Forgekeepers} />
              <Route path='radiantring' component={Radiant} />
            </Route>
            <Route path='gathering'>
              <Route path='prospectorsunion' component={Prospectors} />
              <Route path='stonecutters' component={Stonecutters} />
            </Route>
          </Route>

          <Route path='*' component={NotFound} />
        </Route>
      </Router>
    )
  }
}
