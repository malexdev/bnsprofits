const express = require('express')
const compress = require('compression')
const root = require('root-path')

// load lib modules
const files = require(root('lib', 'files'))
const api = require(root('lib', 'api'))

// set up app
const app = express()
app.use(compress())
app.use(express.static(__dirname + '/public'))
app.set('view engine', 'jade')

// set up lib modules
files.init(app)
api.init(app)

// render any other request to index
app.get('*', (req, res) => {
  res.render('index')
})

// start the server
app.listen(process.env.PORT || 3000)
console.log('Server running')
