const root = require('root-path')
const express = require('express')

// load api subpoints
const item = require(root('lib', 'api', 'item'))
const order = require(root('lib', 'api', 'order'))

// set up the api router
function init(app) {
  const api = express.Router()

  item.init(api)
  order.init(api)

  app.use('/api', api)
}

module.exports = {
  init
}
