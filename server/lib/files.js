const path = require('path')
const fs = require('fs')

const rollups = {
  css: path.join(__dirname, 'public', 'css'),
  js: path.join(__dirname, 'public', 'js')
}

function readFile(path, mode) {
  return new Promise((resolve, reject) => {
    fs.readFile(path, mode, (err, content) => {
      if (err) { return reject(err); }
      resolve(content);
    });
  });
}

// load the specified targets and serve them to the provided response object
function rollupFiles(targets, contentType, res) {
  Promise.all(targets.map(loadFile)).then(files => {
    res.set('Content-Type', contentType)
    res.send(files.join(''))
  }).catch(err => {
    res.status(500).send(err)
  })
}

// load the given file defaulting to utf8
function loadFile(filepath) {
  return readFile(filepath, 'utf8')
}

function init(app) {
  // seperate files with +
  app.get('/css/rollup/:files', (req, res) => {
    let files = req.params.files
      .split('+') // split on + signs
      .map(x => /\.css$/.test(x) ? x : `${x}.css`) // add .css to the end of the file if not present
      .map(x => path.join(rollups.css, x)) // join the file name to the css directory

    rollupFiles(files, 'text/css', res)
  })

  // seperate files with +
  app.get('/js/rollup/:files', (req, res) => {
    let files = req.params.files
      .split('+')
      .map(x => /\.js/.test(x) ? x : `${x}.js`)
      .map(x => path.join(rollups.js, x))

    rollupFiles(files, 'text/javascript', res)
  })
}

module.exports = {
  init
}
