const chance = require('chance')

class Order {
  constructor(name) {
    this.guid = chance.guid()
    this.name = name
    this.icon = ''
    this.description = ''

    this.ingredients = {} // a key value of {'guid': <number of said guid>}. guid values should be instances of Item classes.
    this.time = 0 // the number of milliseconds it takes to craft
    this.output = '' // the guid of the Item class that is output.
    this.quantity = 0 // the number of Items that each order results in.
    this.cost = 0 // the number of copper it costs to craft this Order

    this.level = 0 // the guild level required for the Order
  }
}

module.exports = Order
