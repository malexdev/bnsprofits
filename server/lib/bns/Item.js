const chance = require('chance')

class Item {
  constructor(name) {
    this.guid = chance.guid()
    this.name = name
    this.icon = ''
    this.description = ''
  }
}

module.exports = Item
