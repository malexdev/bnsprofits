class Currency {
  constructor(amounts) {
    this.gold = amounts.gold || 0
    this.silver = amounts.silver || 0
    this.copper = amounts.copper || 0
  }
}

module.exports = Currency
