const Promise = require('bluebird')
const root = require('root-path')

// for now mock out the db
const _items = {}

// item CRUD
const items = {
  list() {
    return Promise.resolve(Object.keys(_items).map(key => _items[key]))
  },
  get(id) {
    return Promise.resolve(_items[id])
  },
  add(item) {
    _items[item.guid] = item
    return Promise.resolve(_items)
  },
  update(id, item) {
    _items[id] = item
    return Promise.resolve(_items)
  },
  remove(id) {
    delete _items[id]
    return Promise.resolve(_items)
  }
}

const orders = {

}

module.exports = {
  items,
  orders
}
