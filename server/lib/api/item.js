const root = require('root-path')
const db = require(root('lib', 'db')).items
const express = require('express')

function init(app) {
  const api = express.Router()

  api.get('/', (req, res) => {
    db.list().then(items => {
      res.status(200).send(items)
    }).catch(err => {
      console.log('Unable to list items: ', err)
      res.status(500).send()
    })
  })

  api.get('/:id', (req, res) => {
    db.get(req.params.id).then(item => {
      if (item) {
        res.status(200).send(item)
      } else {
        res.status(404).send()
      }
    }).catch(err => {
      console.log('Unable to list item %s: ', req.params.id, err)
      res.status(500).send()
    })
  })

  api.post('/', (req, res) => {
    db.add(req.body).then(() => {
      res.status(200).send()
    }).catch(err => {
      console.log('Unable to add item %j: ', req.body, err)
      res.status(500).send()
    })
  })

  api.post('/:id', (req, res) => {
    db.update(req.params.id, req.body).then(() => {
      res.status(200).send()
    }).catch(err => {
      console.log('Unable to update item %s to %j: ', req.params.id, req.body, err)
      res.status(500).send()
    })
  })

  api.delete('/:id', (req, res) => {
    db.remove(req.params.id).then(() => {
      res.status(200).send()
    }).catch(err => {
      console.log('Unable to delete item %s: ', req.params.id, err)
      res.status(500).send()
    })
  })

  app.use('/items', api)
}

module.exports = {
  init
}
