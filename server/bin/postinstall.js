const path = require('path');
const exec = require('child_process').execSync;

console.log('Rebuilding client...');
exec('npm run build-prod', {
  cwd: path.resolve(path.join('..', 'client'))
});
